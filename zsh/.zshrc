#!/bin/zsh
setopt extended_glob

export TERM="xterm-256color"
# If you come from bash you might have to change your $PATH.
# PATH here includes:
# - the base system path
# - executables for PHP Composer
# - executables for NodeJS yarn
# - executables for Go
# - executables for Ruby
# - user executables under $HOME/bin
export PATH="$HOME/.composer/vendor/bin:$HOME/.yarn/bin:$PATH"
export PATH="$HOME/.config/yarn/global/node_modules/.bin:$HOME/go/bin:$PATH"
export PATH="$HOME/.config/composer/vendor/bin:$PATH"
export PATH="$(ruby -e 'puts Gem.user_dir')/bin:$HOME/bin:$PATH"

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
POWERLEVEL9K_MODE="nerdfont-complete"
ZSH_THEME="powerlevel9k/powerlevel9k"

# Load wal colorscheme
cat ~/.cache/wal/sequences

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git docker kubectl sudo zsh-syntax-highlighting)

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(user host dir vcs)

prompt_conditional_go_version() {
    if [[ "${PWD##$GOPATH}" != "$PWD" ]]; then
        "$1_prompt_segment" "$0" "$2" "green" "grey93" "`go version 2>/dev/null | sed -E "s/.*(go[0-9.]*).*/\1/"`" "GO_ICON"
    fi
}

prompt_conditional_node_version() {
    if [ -d (../)#node_modules ]; then
        "$1_prompt_segment" "$0" "$2" "green" "black" "`node -v`" 'NODE_ICON';
    fi
}

prompt_conditional_php_version() {
    if [ -f (../)#composer.json ]; then
        "$1_prompt_segment" "$0" "$2" "fuchsia" "grey93" "`php -v 2>&1 | grep -oe "^PHP\s*[0-9.]*"`"
    fi
}

POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status conditional_go_version conditional_node_version conditional_php_version  virtualenv time)

POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_RPROMPT_ON_NEWLINE=true

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
export GOPATH="$HOME/go"

#export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
# This is nvm for ArchLinux from the AUR.
source /usr/share/nvm/init-nvm.sh

# The fuck?
eval "$(thefuck --alias)"

PATH="/home/ttpcodes/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/ttpcodes/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/ttpcodes/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/ttpcodes/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/ttpcodes/perl5"; export PERL_MM_OPT;

export DOTNET_CLI_TELEMETRY_OPTOUT=1
export EDITOR=vim

alias gotrash=/home/ttpcodes/go/bin/trash
