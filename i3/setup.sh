#!/bin/bash
# One liner for getting the directory the script resides in.
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Install i3-gaps, i3status, and i3blocks.
sudo pacman -S i3
rm -r ~/.config/i3
ln -s $SCRIPT_DIR/i3 ~/.config/i3
