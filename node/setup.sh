#!/bin/bash
echo "Running Node.js environment setup."
git clone https://aur.archlinux.org/nvm.git
cd nvm
makepkg -si
nvm install --lts
echo "Node.js setup completed!"
